#!/usr/bin/env python2
#
# Example to classify faces.
# Brandon Amos
# 2015/10/11
#
# Copyright 2015-2016 Carnegie Mellon University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import time
from time import gmtime, strftime

start = time.time()

import argparse
import cv2
import os
import pickle
import json
import sys
import requests

from operator import itemgetter

import numpy as np
np.set_printoptions(precision=2)
import pandas as pd

import openface

from sklearn.pipeline import Pipeline
from sklearn.lda import LDA
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC
from sklearn.grid_search import GridSearchCV
from sklearn.mixture import GMM
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB

fileDir = os.path.dirname(os.path.realpath(__file__))
modelDir = os.path.join(fileDir, '/','root', 'openface', 'models')
dlibModelDir = os.path.join(modelDir, 'dlib')
openfaceModelDir = os.path.join(modelDir, 'openface')

def extractFaces(image, imageDir):
    start = time.time()
    rgbImg = image

    if(not os.path.exists(imageDir)):
        os.makedirs(imageDir)

    if args.verbose:
        print("Loading the image took {} seconds.".format(time.time() - start))

    start = time.time()

    bbs = align.getAllFaceBoundingBoxes(rgbImg)
    if len(bbs) == 0:
        #raise Exception("Unable to find a face")
        return

    if args.verbose:
        print("Face detection took {} seconds.".format(time.time() - start))

    for bb in bbs:
        print "Found a face!"
        print " bb left " + str(bb.left()) + " bb bottom " + str(bb.bottom()) + " bb right " + str(bb.right()) + " bb top " + str(bb.top())
        bl = (bb.left(), bb.bottom())
        tr = (bb.right(), bb.top())

        p1 = [bb.left(),bb.bottom()]
        p2 = [bb.right(),bb.top()]

        print type(rgbImg)
        croppedImage = rgbImg[bb.top()-1:bb.bottom()+1, bb.left()-1:bb.right()+1]

        print("croppedImage size: {}".format(croppedImage.shape))
        label = strftime("%j%H%M%S", gmtime())

        savePath = os.path.join(imageDir, "{}-{}.jpg".format("extractedface", label))
        cv2.imwrite(savePath, croppedImage)


def getRep(image, multiple=False):
    start = time.time()
    rgbImg = image

    if args.verbose:
        print("  + Original size: {}".format(rgbImg.shape))
    if args.verbose:
        print("Loading the image took {} seconds.".format(time.time() - start))

    start = time.time()

    if multiple:
        bbs = align.getAllFaceBoundingBoxes(rgbImg)
    else:
        bb1 = align.getLargestFaceBoundingBox(rgbImg)
        bbs = [bb1]
    if len(bbs) == 0 or (not multiple and bb1 is None):
        #raise Exception("Unable to find a face")
        return
    if args.verbose:
        print("Face detection took {} seconds.".format(time.time() - start))

    reps = []
    for bb in bbs:
        start = time.time()
        alignedFace = align.align(
            args.imgDim,
            rgbImg,
            bb,
            landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
        if alignedFace is None:
            #raise Exception("Unable to align image")
            raise
        if args.verbose:
            print("Alignment took {} seconds.".format(time.time() - start))
            print("This bbox is centered at {}, {}".format(bb.center().x, bb.center().y))

        start = time.time()
        rep = net.forward(alignedFace)
        if args.verbose:
            print("Neural network forward pass took {} seconds.".format(
                time.time() - start))
        reps.append((bb.center().x, rep))
    sreps = sorted(reps, key=lambda x: x[0])
    return sreps

def infer(args, image, multiple=False):
    try:
        reps = getRep(image, multiple)
        if len(reps) > 1:
            print("List of faces in image from left to right")
        for r in reps:
            rep = r[1].reshape(1, -1)
            bbx = r[0]
            start = time.time()
            predictions = clf.predict_proba(rep).ravel()
            maxI = np.argmax(predictions)
            person = le.inverse_transform(maxI)
            confidence = predictions[maxI]
            print("Predict {} @ x={} with {:.2f} confidence.".format(person.decode('utf-8'), bbx,
                                                                         confidence))
            print("Detection Actions: {} with rate {}.".format(actions[person.decode('utf-8')]["url"],
                                                                   actions[person.decode('utf-8')]["threshold"]))
            if confidence >= float(actions[person.decode('utf-8')]["threshold"]):
		print("***** Execting action for {}".format(person.decode('utf-8')))
                r = requests.post( actions[person.decode('utf-8')]["url"] )
                print("Post result ", r.status_code)

            if isinstance(clf, GMM):
                dist = np.linalg.norm(rep - clf.means_[maxI])
                print("  + Distance from the mean: {}".format(dist))
    except:
        #print("Exception.....")
        pass
        #nothing


errcount=0
if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--dlibFacePredictor',
        type=str,
        help="Path to dlib's face predictor.",
        default=os.path.join(
            dlibModelDir,
            "shape_predictor_68_face_landmarks.dat"))
    parser.add_argument(
        '--networkModel',
        type=str,
        help="Path to Torch network model.",
        default=os.path.join(
            openfaceModelDir,
            'nn4.small2.v1.t7'))
    parser.add_argument('--imgDim', type=int,
                        help="Default image dimension.", default=96)
    parser.add_argument('--cuda', action='store_true')
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument('--classifierModel', type=str, help="Path to face classifier pkl file")
    parser.add_argument('--actionsFile', type=str, help="Path to detection actions json file")
    parser.add_argument('--extractMode', action='store_true', help="enter image extract mode, must specify imageDir")
    parser.add_argument('--imageDir', type=str, help="The directory to save extracted faces in.")
    parser.add_argument('--cameraUrl', type=str, help="Url to the web camera")

    args = parser.parse_args()
    if args.verbose:
        print("Argument parsing and import libraries took {} seconds.".format(
            time.time() - start))

    if args.cameraUrl:
	print "Using camera at url: " + args.cameraUrl

    if args.extractMode:
        print "Extract Mode is on"
        print "Extraced face images will be saved to: " + args.imageDir

    else:
        if args.classifierModel.endswith(".t7"):
            raise Exception("""
    Torch network model passed as the classification model,
    which should be a Python pickle (.pkl)

    See the documentation for the distinction between the Torch
    network and classification models:

            http://cmusatyalab.github.io/openface/demo-3-classifier/
            http://cmusatyalab.github.io/openface/training-new-models/

    Use `--networkModel` to set a non-standard Torch network model.""")

    #Load the classifier model
        with open(args.classifierModel, 'rb') as f:
            if sys.version_info[0] < 3:
                    (le, clf) = pickle.load(f)
            else:
                    (le, clf) = pickle.load(f, encoding='latin1')

    if not args.extractMode and args.actionsFile:
        #load the actions file
        with open(args.actionsFile) as actions_file:
            actions = json.load(actions_file)
    else:
	print "Missing actionsFile argument"

    #Load the face detection model
    start = time.time()
    align = openface.AlignDlib(args.dlibFacePredictor)
    net = openface.TorchNeuralNet(args.networkModel, imgDim=args.imgDim,
                                  cuda=args.cuda)

    if args.verbose:
        print("Loading the dlib and OpenFace models took {} seconds.".format( time.time() - start))
        start = time.time()

    #Read from Camera and infer people.
    video_capture = cv2.VideoCapture(args.cameraUrl)
    if ( video_capture.isOpened()):
        print("Camera Opened")
    else:
        print("Error opening camera")

    # print "Frame Rate is ", video_capture.get(5) #5 is the id for FPS

    while True:
        # Grab a single frame of video
        ret, frame = video_capture.read()

        if(ret):
            if args.extractMode:
                extractFaces(frame, args.imageDir)
            else:
                # Resize frame of video to 1/4 size for faster face recognition processing
                small_frame = cv2.resize(frame, (0, 0), fx=0.5, fy=0.5)
        else:
            errcount = errcount + 1
            print("Error reading from webcam")
            time.sleep(1)
            continue

        # Process the frames
        if not args.extractMode:
            infer(args, small_frame, True)
